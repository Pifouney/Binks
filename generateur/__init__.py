# -*- coding: utf-8 -*-:
"""
Created on Tue Oct 17 17:00:00 2017

@author : HOUDAYER Pierre
@author : LIBERGE Corentin

"""
import argparse
import sys
import random
import psycopg2

def run():
    """principal function"""
    cli = argparse.ArgumentParser("Générateur de playlist : Binks")
    cli.add_argument('-d', '--duree', help='Duration of playlist', required=True, type=int)
    cli.add_argument('-n', '--name', help='Name of the file who is created (Format : name.txt)')
    cli.add_argument('-a', '--artiste', help='Tri par artiste dans la playlist', type=str)
    cli.add_argument('-s', '--sous_genre',
                     help='Trie par sous-genre de morceaux dans la playlist',
                     type=str)
    cli.add_argument('-g', '--genre', help='Tri par genre de morceaux dans la playlist', type=str)
    cli.add_argument('-A', '--album', help='Tri par nom d\'album de la playlist', type=str)
    cli.add_argument('-t', '--titre', help='Tri par titre de morceaux dans la playlist', type=str)
    cli.add_argument('-q', '--quantite', help='quantité de morceaux souhaité', type=str)
    cli.add_argument('name', help='Name of the playlist')
    args = cli.parse_args()
    # args for connexion to database
    connect_str = "dbname='radio_libre' user='p.houdayer' host='80.82.238.198' password='P@ssword'"
    # use our connection values to establish a connection
    try:
        # create a psycopg2 cursor that can execute queries
        conn = psycopg2.connect(connect_str)
        cursor = conn.cursor()
    except psycopg2.OperationalError:
        sys.exit(0)
    # second -> minutes
    tempsensecondes = int(args.duree)*60

    # Initialisation

    lespistes = [] # list of tuples
    dureetotal = 0 # duration total of all tracks
    dureemorceau = 0 # duration of one morceau
    cpt = 0
    suffix = "liste"

    # Creation of 3 list : genre, artiste, titre
    if args.genre is not None or args.artiste is not None or args.titre is not None:
        for argument in ['genre', 'artiste', 'titre']:
            if getattr(args, argument) is not None:
                try:
                    cursor.execute(
                        "SELECT * FROM morceaux WHERE %s ~'%s' ORDER BY RANDOM()"
                        % (argument, getattr(args, argument))
                    )
                    setattr(args, argument+suffix, cursor.fetchall())
                    lespistes.extend(getattr(args, argument+suffix))
                except psycopg2.OperationalError:
                    print("Impossible de faire quelque chose")
                    exit(1)

    else:
        cursor.execute("SELECT * FROM morceaux ORDER BY RANDOM()")
        lespistes = cursor.fetchall()

    # Random mixing of tracklist
    random.shuffle(lespistes)

    print("temps en seconde : " + str(tempsensecondes))

    listefinale = []
    #loop as long as the total time is less than time in seconds and cpt
    while dureetotal < tempsensecondes and cpt < len(lespistes):
        dureemorceau = lespistes[cpt][5]

        if dureetotal + dureemorceau <= tempsensecondes:
            dureetotal = dureetotal + lespistes[cpt][5]
            # Track display
            print(lespistes[cpt][0] + " - " + lespistes[cpt][2] + " - "
                  + lespistes[cpt][3] + " - " + str(lespistes[cpt][5]) + " secondes ")
            listefinale.append(lespistes[cpt])

        cpt = (cpt + 1)

    # open files list
    try:
        liste = open(args.name, "w")
    except PermissionError:
        print('impossible d\'ouvrir le fichier')
        sys.exit(0)

        # insert track into the list file

    for information in listefinale:
        try:
            liste.write(str(information[8]) + "\n")
        except PermissionError:
            print('impossible d\'ecrire dans la liste')
            sys.exit(0)

if __name__ == "__main__":
    run()
