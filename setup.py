from setuptools import setup
setup(
    author="HOUDAYER Pierre",
    author_email="pierre.houdayer@bts-malraux.net",
    classifiers=[
        'Development Status :: 1 - Alpha',
        'Environment :: Console',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Natural Language :: English',
        'Operating System :: POSIC :: Linux',
        'Topic :: Multimedia :: Sound/Audio',
        'Topic :: Utilities'
	],
    description="Executable du projet Generateur de Playlist",
    entry_points={
        'console_scripts': [
            'generateur-executable = generateur:run'
            ]
        },
        install_requires=["psycopg2"],
        keywords='radio playlist generator',
        license="GPLv3+",
        long_description="Aucune inspiration pour rédiger cette longue vraiment longue description.",
        name="generateur",
        packages=['generateur'],
	data_files=[] 
        url="http://bts.bts-malraux.net/~p.houdayer",
        version="1.0"
)
